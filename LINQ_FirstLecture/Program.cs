﻿using LINQ_FirstLecture.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Linq;

using LINQ_FirstLecture.Services;

namespace LINQ_FirstLecture
{
    public  class Program
    {
        static void Main(string[] args)
        {
            MainWindowManager manager = new MainWindowManager();
            manager.Start();
            Console.Read();
        }

    }
}
