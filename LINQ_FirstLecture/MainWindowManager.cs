﻿using LINQ_FirstLecture.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ_FirstLecture
{
    public class MainWindowManager
    {
        private readonly QueryService _service;
        private string MainMenu = "1 - Get dictionary(key - project, value - count of tasks) by user id\n" +
            "2 - List of tasks for user\n" +
            "3 - Get (id, name) from list of tasks which are finished by user id\n" +
            "4 - Get list (id, team name and list of users)  from list of teams\n" +
            "5 - Get list of users by first_name with sorted tasks by name length\n" +
            "6- Get CombinedInfoAboutUserTask6\n" +
            "7- Get CombinedInfoAboutProjectTask7\n" +
            "-1 to exit\n";
        public MainWindowManager()
        {
            _service = new QueryService();
        }

        public int GetId()
        {
            int.TryParse(Console.ReadLine(), out int id);
            while(id == 0)
            {
                Console.WriteLine("Enter id once more");
                int.TryParse(Console.ReadLine(), out id);
            }
            Console.Clear();
            return id;
        }

        public int GetAnswer()
        {
            Console.WriteLine("Enter your answer id");
            int.TryParse(Console.ReadLine(), out int id);
            if (id == -1)
                System.Environment.Exit(-1);

            while(id < 1 || id > 7)
            {
                Console.WriteLine("Enter correct data");
                int.TryParse(Console.ReadLine(), out id);
            }

            Console.Clear();
            return id;
        }

        public void WantToContinue()
        {
            Console.WriteLine("\n\nDo you want to go on ?");
            string answer = Console.ReadLine();
            if (answer == "y" || answer == "Y")
                Console.Clear();
            else
                System.Environment.Exit(-2);
        }

        public void Start()
        {
            Console.WriteLine(MainMenu);
            int answer = GetAnswer();
            while(answer != -1)
            {
                switch (answer)
                {
                    case 1:
                        {
                            DisplayDictionaryProjectToTaskCountByUserId();
                            WantToContinue();
                            break;
                        }
                    case 2:
                        {
                            DisplayTasksByUserIdWhereNameLessThan45();
                            WantToContinue();
                            break;
                        }
                    case 3:
                        {
                            DisplayTupleFromCollectionOfTasksWhichAreFinished();
                            WantToContinue();
                            break;
                        }
                    case 4:
                        {
                            DisplayTeamsOlderThan10Years();
                            WantToContinue();
                            break;
                        }
                    case 5:
                        {
                            DisplayListOfUsers();
                            WantToContinue();
                            break;
                        }
                    case 6:
                        {
                            GetTask6Structure();
                            WantToContinue();
                            break;
                        }
                    case 7:
                        {
                            GetTask7Structure();
                            WantToContinue();
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("Incorect answer");
                            WantToContinue();
                            break;
                        }
                }

                Console.WriteLine(MainMenu);
                answer = GetAnswer();
            }
            Console.WriteLine("Bye - bye");
        }

        

        public void DisplayDictionaryProjectToTaskCountByUserId()
        {
            Console.WriteLine("Enter user id");
            var result = _service.GetDictionaryProjectToTaskCountByUserId(GetId());
            foreach(var item in result)
            {
                Console.WriteLine($"Project: {item.Key.Name}, Task count: {item.Value}");
            }
        }

        public void DisplayTasksByUserIdWhereNameLessThan45()
        {
            Console.WriteLine("Enter user id");
            var result = _service.GetTasksByUserIdWhereNameLessThan45(GetId());
            foreach (var item in result)
            {
                Console.WriteLine($"Task name: {item.Name}, task id: {item.Id}");
            }
        }

        public void DisplayTupleFromCollectionOfTasksWhichAreFinished()
        {
            Console.WriteLine("Enter user id");
            var result = _service.GetTupleFromCollectionOfTasksWhichAreFinished(GetId());
            foreach (var item in result)
            {
                Console.WriteLine($"Finished task: Name = {item.name}, Id = {item.id}");
            }
        }

        public void DisplayTeamsOlderThan10Years()
        {
            var result = _service.GetTeamsOlderThan10Years();
            foreach (var item in result)
            {
                Console.WriteLine($"id = {item.teamId}  name = {item.teamName}");
                foreach(var user in item.users)
                {
                    Console.WriteLine($"User name: {user.firstName}");
                }
            }
        }

        public void DisplayListOfUsers()
        {
            var result = _service.GetListOfUsers();
            foreach (var item in result)
            {
                Console.WriteLine($"id = {item.Item1.Id}  name = {item.Item1.firstName}");
                foreach (var task in item.Item2)
                {
                    Console.WriteLine($"Task name {task.Name}");
                }
            }
        }

        public void GetTask6Structure()
        {
            Console.WriteLine("Enter user id");
            var result = _service.GetInfoAboutUser(GetId());
            Console.WriteLine(JsonConvert.SerializeObject(result));
        }

        public void GetTask7Structure()
        {
            var result = _service.GetProjectInfo();
            Console.WriteLine(JsonConvert.SerializeObject(result));
        }

    }
}
