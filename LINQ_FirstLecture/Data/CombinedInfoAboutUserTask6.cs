﻿using LINQ_FirstLecture.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ_FirstLecture.Data
{
    public class CombinedInfoAboutUserTask6
    {
        public User User{ get; set; }
        public Project LastProject { get; set; }
        public int CountOfTasks { get; set; }
        public int CountOfUnfinishedOrCanceledTasks { get; set; }
        public Task_ LongestTask { get; set; }
    }
}
