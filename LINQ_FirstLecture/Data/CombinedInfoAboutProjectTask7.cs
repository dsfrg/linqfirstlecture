﻿using LINQ_FirstLecture.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ_FirstLecture.Data
{
    public class CombinedInfoAboutProjectTask7
    {
        public Project Project { get; set; }
        public Task_ LongestTaskByDescription { get; set; }
        public Task_  ShortestTaskByName { get; set; }
        public int CountOfUsers { get; set; }
    }
}
