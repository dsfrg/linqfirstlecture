﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LINQ_FirstLecture
{
    public static class HttpService
    {
        const string hostName = @"https://bsa20.azurewebsites.net/api/";

        public async static Task<string> GetStringResponce(string api)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var responseBody = await httpClient.GetAsync(hostName + api);
                if (!responseBody.IsSuccessStatusCode)
                {
                    Console.WriteLine("Server is not available");
                    throw new InvalidOperationException("No responce");
                }
                return await responseBody.Content.ReadAsStringAsync();
            }
        }
    }
}
