﻿using LINQ_FirstLecture.Data;
using LINQ_FirstLecture.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace LINQ_FirstLecture.Services
{
    public class QueryService
    {
        #region Data
        public List<Project> Projects { get; set; }
        public List<Task_> Tasks { get; set; }
        public List<Team> Teams { get; set; }
        public List<User> Users { get; set; }
        #endregion

        #region BasicInit
        public QueryService()
        {
            Teams = new List<Team>(JsonConvert.DeserializeObject<List<Team>>(HttpService.GetStringResponce("Teams").Result));
            Users = new List<User>(JsonConvert.DeserializeObject<List<User>>(HttpService.GetStringResponce("Users").Result));
            Projects = new List<Project>(JsonConvert.DeserializeObject<List<Project>>(HttpService.GetStringResponce(@"Projects").Result));
            Tasks = new List<Task_>(JsonConvert.DeserializeObject<List<Task_>>(HttpService.GetStringResponce("Tasks").Result));
        }
        #endregion

        #region FirstTask
        public Dictionary<Project, int> GetDictionaryProjectToTaskCountByUserId(int userId)
        {
            var projectToTaskCount = Tasks.GroupBy(x => x.projectId)
                .Join(
                    Projects, 
                    task => task.Key, 
                    project => project.Id,
                    (task, project) => new
                    {
                        project = new Project()
                        {
                            Id = project.Id,
                            Name = project.Name,
                            Description = project.Description,
                            createdAt = project.createdAt,
                            deadline = project.deadline,
                            authorId = project.authorId,
                            user = Users.FirstOrDefault(x => x.Id == project.authorId),
                            teamId = project.teamId,
                            Team = Teams.FirstOrDefault(x => x.Id == project.teamId)
                        },
                        count = task.Count()
                    })
                .Where(x => x.project.authorId == userId);


            Dictionary<Project, int> result = new Dictionary<Project, int>();
            foreach (var item in projectToTaskCount)
            {
                result.Add(item.project, item.count);
            }
            return result;
        }
        #endregion

        #region SecondTask
        public List<Task_> GetTasksByUserIdWhereNameLessThan45(int userId)
        {
            return Tasks
                .Join(
                        Users,
                        task => task.performerId,
                        u => u.Id,
                        (task, user) => new Task_()
                        {
                            Id = task.Id,
                            Name = task.Name,
                            Description = task.Description,
                            createdAt = task.createdAt,
                            finishedAt = task.finishedAt,
                            state = task.state,
                            projectId = task.projectId,
                            project = Projects.FirstOrDefault(x => x.Id == task.projectId),
                            performerId = task.performerId,
                            User = user
                        })
                .Where(task => task.User.Id == userId && task.Name.Length < 45).ToList();
        }
        #endregion

        #region ThirdTask
        public List<(int id, string name)> GetTupleFromCollectionOfTasksWhichAreFinished(int userId)
        {
            return Tasks.Join(
                        Users,
                        task => task.performerId,
                        u => u.Id,
                        (task, user) => new Task_()
                        {
                            Id = task.Id,
                            Name = task.Name,
                            Description = task.Description,
                            createdAt = task.createdAt,
                            finishedAt = task.finishedAt,
                            state = task.state,
                            projectId = task.projectId,
                            project = Projects.FirstOrDefault(x => x.Id == task.projectId),
                            performerId = task.performerId,
                            User = user
                        })
                .Where(x => x.User.Id == userId && x.state == TaskState.Finished && x.finishedAt.Year == 2020)
                .Select(x => ( id: x.Id, name: x.Name))
                .ToList();
        }
        #endregion

        #region FourthTask

        public List<(int teamId, string teamName, List<User> users)> GetTeamsOlderThan10Years()
        {
            return Users
                .Join(
                    Teams,
                    user => user.TeamId,
                    team => team.Id,
                    (user, team) => new User()
                    {
                        Id = user.Id,
                        firstName = user.firstName,
                        lastName = user.lastName,
                        Email = user.Email,
                        Birthday = user.Birthday,
                        registeredAt = user.registeredAt,
                        TeamId = user.TeamId,
                        Team = team
                    })
                .Where(x => (DateTime.Now - x.Birthday).Days / 365 > 10)
                .OrderByDescending(x => x.registeredAt)
                .GroupBy(x => x.Team)
                .Select(x => (x.Key.Id, x.Key.Name, x.ToList()))
                .ToList();
        }
        #endregion

        #region FifthTask
        public List<(User, List<Task_>)> GetListOfUsers()
        {
            return  Tasks
                .Join(
                        Users,
                        task => task.performerId,
                        u => u.Id,
                        (task, user) => new Task_()
                        {
                            Id = task.Id,
                            Name = task.Name,
                            Description = task.Description,
                            createdAt = task.createdAt,
                            finishedAt = task.finishedAt,
                            state = task.state,
                            projectId = task.projectId,
                            project = Projects.FirstOrDefault(x => x.Id == task.projectId),
                            performerId = task.performerId,
                            User = user
                        }
                     )
                .OrderBy(x => x.User.firstName)
                .GroupBy(x => x.User)
                .Select(x => (x.Key, x.OrderByDescending(x => x.Name.Length).ToList()))
                .ToList();

        }
        #endregion

        #region SixthTask
        public CombinedInfoAboutUserTask6 GetInfoAboutUser(int userId)
        {
            return Users
                .Where(x => x.Id == userId)
                .Join(Projects,
                user => user.Id,
                project => project.authorId,
                (user, project) => new Project()
                {
                    Id = project.Id,
                    Name = project.Name,
                    Description = project.Description,
                    createdAt = project.createdAt,
                    deadline = project.deadline,
                    authorId = project.authorId,
                    user = user,
                    teamId = project.teamId,
                    Team = Teams.FirstOrDefault(x => x.Id == project.teamId)
                })
                .Join(Tasks,
                project => project.authorId,
                task => task.performerId,
                (project, task) => new Task_()
                {
                    Id = task.Id,
                    Name = task.Name,
                    Description = task.Description,
                    createdAt = task.createdAt,
                    finishedAt = task.finishedAt,
                    state = task.state,
                    projectId = task.projectId,
                    project = project,
                    performerId = task.performerId,
                    User = project.user
                })
                .Select(x => new CombinedInfoAboutUserTask6()
                {
                    User = x.User,
                    LastProject = Projects.Find(q => q.createdAt == Projects.Where(p => p.authorId == x.User.Id).Max(z => z.createdAt)),
                    CountOfTasks = Tasks.Count(t => t.performerId == x.performerId),
                    CountOfUnfinishedOrCanceledTasks = Tasks.Count(t => t.state != TaskState.Finished && t.performerId == x.performerId),
                    LongestTask = Tasks.Where(t => t.performerId == x.performerId).OrderByDescending(t => t.finishedAt - t.createdAt).FirstOrDefault()
                })
                .FirstOrDefault();
        }

        #endregion

        #region SeventhTask

        public List<CombinedInfoAboutProjectTask7> GetProjectInfo()
        { 
            return Projects.Join(
                Tasks,
                project => project.Id,
                task => task.projectId,
                (project, task) => new Task_()
                {
                    Id = task.Id,
                    Name = task.Name,
                    Description = task.Description,
                    createdAt = task.createdAt,
                    finishedAt = task.finishedAt,
                    state = task.state,
                    projectId = task.projectId,
                    project = project,
                    performerId = task.performerId,
                    User = Users.Find(x => x.Id == project.authorId)
                })
                .GroupBy(x => x.project)
                .Select(x => new CombinedInfoAboutProjectTask7()
                {
                    Project = x.Key,
                    LongestTaskByDescription = Tasks.Where(t => t.projectId == x.Key.Id).OrderBy(t => t.Description).First(),
                    ShortestTaskByName = Tasks.Where(t => t.projectId == x.Key.Id).OrderBy(t => t.Name).Last(),
                    CountOfUsers = Users.Where(u => u.TeamId == x.Key.teamId && (Tasks.Count(q => q.projectId == x.Key.Id) < 3 || x.Key.Description.Length > 20)).Count()
                })
                .ToList();
        }
        #endregion
    }
}
