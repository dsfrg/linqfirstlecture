﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ_FirstLecture.Entities
{
    public enum TaskState
    {
        Created,
        Started,
        Finished,
        Canceled
    }
}
