﻿using System;
using System.Collections.Generic;

namespace LINQ_FirstLecture.Entities
{
    public class Task_
    {
        public int Id{ get; set; }
        public string? Name{ get; set; } 
        public string? Description { get; set; }

        public DateTime createdAt { get; set; }
        public DateTime finishedAt { get; set; }
        public TaskState state { get; set; }

        public  int projectId { get; set; }
        public Project project { get; set; }
        public int performerId { get; set; }
        public User User { get; set; }
    }
}
