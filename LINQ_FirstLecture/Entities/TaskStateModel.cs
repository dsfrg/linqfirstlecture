﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ_FirstLecture.Entities
{
    public class TaskStateModel
    {
        public  int Id { get; set; }
        public string Value { get; set; }
    }
}
