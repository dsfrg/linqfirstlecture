﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ_FirstLecture.Entities
{
    public class Team
    {
        public int Id { get; set; }
        public string? Name  { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
