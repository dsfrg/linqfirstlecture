﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ_FirstLecture.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string? firstName { get; set; }
        public string?  lastName { get; set; }
        public string? Email { get; set; }

        public DateTime Birthday { get; set; }
        public DateTime registeredAt { get; set; }

        public int? TeamId { get; set; }
        public Team Team { get; set; }
    }
}
