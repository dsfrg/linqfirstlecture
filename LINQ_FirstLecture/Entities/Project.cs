﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ_FirstLecture.Entities
{
    public class Project
    {
        public int Id { get; set; }
        public string? Name { get; set; }

        public string? Description { get; set; }

        public DateTime createdAt { get; set; }
        public DateTime deadline { get; set; }

        public int authorId { get; set; }
        public User user { get; set; }

        public int  teamId { get; set; }
        public Team  Team { get; set; }

    }
}
